
#pragma comment(linker,"/STACK:16777216")
#pragma  warning ( disable: 4786)
#include <bits/stdc++.h>
using namespace std;
#define max(x,y) ((x)>(y)?(x):(y))
#define min(x,y) ((x)<(y)?(x):(y))
#define forl(i,a,b) for ( i = a; i < b; i++)
#define pb push_back
#define in(a,b,c) ((a) <= (b) && (b) <= (c))
#define ms(a,b) memset((a),(b),sizeof(a))
#define all(v) (v).begin(),(v).end()
#define pb push_back
typedef vector<int> vi;
typedef pair<int,int> pii;
typedef vector<pii> vpii;

#define MAX 100005
#define MAX_QUESTION 500
#include "constants.h"
#include "preference_relation.h"
#include "dominance_relation.h"

int ground_truth[CRITERIA_SIZE][OBJECT_SIZE][OBJECT_SIZE];
//ground_truth[c][x][y] is 0 if y is better than x on criterion c
vector<int> objects;
int n; //object size
vector<int> criteria;
vector<int> asked_questions[MAX_QUESTION];
set<int> remaining_objects; //O?
set<int> notPareto; //Ox
set<int> pareto;
bool includeNon_pareto;
ofstream croud_result_output;
//array of graphs, one graph for each criterion
preference_graph *graphs;
dominance_graph dgraph; //among objects
int getCriteriaSize(int,int);
/**for (x,y) question
return 0 if x>y, x dominates y
1 if x<y
2 for indifference x~y
*/
int CroudResult(){
    //int x_count=0;
    //int y_count=0;
    //int i;
    int indifferent=0;
    int r;
    /*forl(i,0,CROUD_SIZE){
        r = rand()%3;
        x_count += r==0;
        y_count += r==1;
        indifferent += r==2;
    }
    double a = x_count*1.0/CROUD_SIZE;
    double b = y_count*1.0/CROUD_SIZE;
    double c = indifferent*1.0/CROUD_SIZE;
    if(a>b && a>c)
        return PREFERRED_OVER;
    else if(b>a && b>c)
        return NOT_PREFERRED_OVER;
    else
        return INDIFFERENT_ON_C;*/
    r = rand()%3;
    if(r==0)
        return PREFERRED_OVER;
    else if(r==1)
        return NOT_PREFERRED_OVER;
    else
        return INDIFFERENT_ON_C;
}
struct question_pair{
    int fir;
    int sec;
};
bool operator<(const question_pair& p1, const question_pair& p2)
{
   int c1 = getCriteriaSize(p1.fir,p1.sec);
   int c2 = getCriteriaSize(p2.fir,p2.sec);
   if(c1 != c2){
        return c1 > c2; //lower |C(x,y)| first
   }else{
        int x1 = dgraph.get_domination_over(p1.fir).size();
        int x2 = dgraph.get_domination_over(p2.fir).size();
        if(x1 != x2){
            return x1 > x2; //lower x first
        }else{
            int y1 = dgraph.get_domination_over(p1.sec).size();
            int y2 = dgraph.get_domination_over(p2.sec).size();
            return y1 < y2; //higher y first
        }
   }
}
int getElementAt(set<int> st, int index){
    set<int>::iterator it;
    int i;
    for(it = st.begin(),i=0; it != st.end(); it++,i++){
        if(i == index){
            return *it;
        }
    }
}
set<int> get_y_set(){
    set<int>::iterator it;
    set<int> st = pareto;
    for(it = remaining_objects.begin(); it != remaining_objects.end(); it++){
        st.insert(*it);
    }
    if(includeNon_pareto){
        //choose from non-pareto
        for(it = notPareto.begin(); it != notPareto.end(); it++){
            st.insert(*it);
        }
    }
    return st;
}

vector<int> generate_question_RandomQ(){
      int i,x,y;
      vector<int> v;
      int totalTryCount = OBJECT_SIZE*OBJECT_SIZE*CRITERIA_SIZE*1000;
      set<int> st = get_y_set();
      while(totalTryCount--){
          //int index = ;
          x = getElementAt(remaining_objects, rand()%remaining_objects.size()); //x must belong to O?
          y = getElementAt(st,rand()%st.size());
          while(x == y){
               x = getElementAt(remaining_objects, rand()%remaining_objects.size()); //x must belong to O?
               y = getElementAt(st,rand()%st.size());
               //cout << remaining_objects.size() << " " << st.size() << " " << x << " " << y << " Stuck here?\n";
          }
          bool ok=true;
          forl(i,0,CRITERIA_SIZE){
              if(graphs[i].preference_checkFrom2D(x,y)){
                  ok=false;
                  break;
              }
          }
          if(ok==false and totalTryCount > 5){
             continue;
          }

          int selected_criteria = rand()%CRITERIA_SIZE;
          //definition 3(i) of Page 5 in paper
          if(graphs[selected_criteria].doesRelationExist(x,y) || graphs[selected_criteria].doesRelationExist(y,x))
               continue;
          v.pb(x),v.pb(y),v.pb(selected_criteria);
          return v;
      }
      return vector<int>();
}

pii getRandomPair(){ //(x,y) pair
    set<int> st= get_y_set();
    int x,y;
    x = getElementAt(remaining_objects, rand()%remaining_objects.size());
    y = getElementAt(st,rand()%st.size());
    while(x == y){
        y = getElementAt(st,rand()%st.size());
    }
    return make_pair(x,y); //x?y
}
/**
select a criterion on pair (x,y)
return -1 if no criterion is available to pick up
*/
int getCriterion_OnPair(int x, int y){
    int c;
    forl(c,0,CRITERIA_SIZE){
        if(graphs[c].doesRelationExist(x,y) || graphs[c].doesRelationExist(y,x))
               continue;
        return c;
    }
    return -1;
}

pii getFRQ_pair(){
    set<int> st = get_y_set();
    set<int> xset = remaining_objects;
    set<int>::iterator it,it1;
    priority_queue<question_pair> pq;
    struct question_pair q_p;
    for(it = xset.begin(); it != xset.end(); it++){
        for(it1 = st.begin(); it1 != st.end(); it1++){
            if(*it == *it1)
                continue;
            if(getCriterion_OnPair(*it,*it1) == -1)
                continue;
            q_p.fir = *it;
            q_p.sec = *it1;
            pq.push(q_p);
        }
    }
    return make_pair(pq.top().fir, pq.top().sec);
}


//return |C(x,y)|
int getCriteriaSize(int x, int y){
    int cnt = 0,c;
    forl(c,0,CRITERIA_SIZE){
        if(graphs[c].doesRelationExist(x,y) || graphs[c].doesRelationExist(y,x))
               continue;
        cnt++;
    }
    return cnt;
}

bool resolve_if_needed(int arr[3], int criterion){
    int z,x,y,i;
    for(i=0;i<3;i++){ //every possible order
        int id1 = i;
        int id2 = (i+1)%3;
        int id3 = (i+2)%3;
        x = arr[id1], y = arr[id2], z = arr[id3];
        if(graphs[criterion].indifference_checkFrom2D(x,y) && graphs[criterion].preference_checkFrom2D(y,z) && graphs[criterion].preference_checkFrom2D(z,x)){
            graphs[criterion].set_relation(z,x, INDIFFERENT_ON_C);
            //croud_result_output << char('a'+x) << " " << char('a'+y) << " " << char('a'+z) << " " << criterion << endl;
            return true;
            //croud_result_output << "with resolve: " << char('a' + z) << " ~ " << char('a' +x) << " over criterion " <<  criterion << endl;
        }
    }
    return false;
}
//rule 2 page 6
void resolve_contradiction(vector<int> v){
    int y,criterion = v[2];
    int arr[3];
    forl(y,0,OBJECT_SIZE){
        if(y != v[0] and y != v[1]){
            arr[0] = v[0], arr[1] = v[1], arr[2] = y;
            int r = resolve_if_needed(arr, criterion);
            if(r == true)
                continue;
            arr[0] = v[1], arr[1] = v[0], arr[2] = y;
            resolve_if_needed(arr, criterion);
        }
    }
}

void print(vector<int> v){
    cout << char('a'+v[0]) << " ? " << char('a'+v[1]) << " over " << v[2] << endl;
}

/**
does y dominates x considering all criteria
*/
int checkIf_OneDominates_Another(int y, int x){
    bool atLeastOne=false;
    bool allCriteriaConsidered = true;
    bool isOpposite = false, all_INDIFFERENT=true;
    int c;
    int dvalue = dgraph.get_dominance(y,x); //earlier calculated value
    if(dvalue != -1){
        if(dvalue == DOMINATE)
            return 1;
        return 0;
    }

    forl(c,0,CRITERIA_SIZE){
        int v1 = graphs[c].getPreferenceValue(y,x);
        int v2 = graphs[c].getPreferenceValue(x,y);
        if(v1 == PREFERRED_OVER){
            atLeastOne = true;
            all_INDIFFERENT = false;
        }else{
            if(v2 == PREFERRED_OVER){
                isOpposite = true;
                all_INDIFFERENT = false; //current object y can't dominate x, because x dominates y by criterion c
            }
            else if(v1 == -1){
                allCriteriaConsidered = false; //no relation has been established yet between x and y by criterion c
                all_INDIFFERENT = false;
            }
        }
    }
    if(all_INDIFFERENT or (atLeastOne && isOpposite)){//indifferent
        dgraph.set_dominance(y,x,INDIFFERENT);
        return 0;
    }
    if(allCriteriaConsidered == false ){
        return NOT_ENOUGH_INFO;
    }
    if(atLeastOne && !isOpposite){
        dgraph.set_dominance(y,x,DOMINATE);
        return 1;
    }
    dgraph.set_dominance(y,x,DOMINATED);
    return 0;
}

pair<bool,int> checkIfNon_pareto_optimal(int x){
    int y;
    pii p1;
    forl(y,0,OBJECT_SIZE){
        if(y==x)
            continue;
        int result = checkIf_OneDominates_Another(y,x);
        if(result==1){
            return make_pair(true,y); //y dominates x
        }
    }
    return make_pair(false,-1);
}

bool checkIfPareto_Optimal(int x){
    int y;
    forl(y,0,OBJECT_SIZE){
        if(y==x)
            continue;
        int result = checkIf_OneDominates_Another(y,x);
        if(result==1 || result == NOT_ENOUGH_INFO) //x can't be pareto optimal
            return false;
    }
    return true;
}
bool isSamePair(pii p1, pii p2){
    if(p1.first == p2.first && p1.second == p2.second)
        return true;
    if(p1.first == p2.second && p1.second == p2.first)
        return true;
    return false;
}
void print_pair(pii p){
    cout << char('a'+p.first) << " " << char('a'+p.second) << endl;
}
bool NeedNon_pareto(){
    vector<int> v(remaining_objects.begin(), remaining_objects.end());
    vector<int> v1 = v; //v1 = (O? and Pareto)
    set<int>::iterator it;
    for(it=pareto.begin();it!= pareto.end();it++){
        v1.pb(*it);
    }
    int sz=v.size(),i,j,sz1=v1.size();
    forl(i,0,sz){
        forl(j,0,sz1){
            if(v[i]==v1[j])
                continue;
            int c = getCriterion_OnPair(v[i],v1[j]);
            if(c != -1){ //got a question to ask
                return false;
            }
        }
    }
    return true;
}
int score_comp(pii p1, pii p2){
    if(p1.second > p2.second)
        return 1;
    return 0;
}
int main(void)
{
    //freopen("1.txt", "r", stdin);
    //freopen("2.txt", "w", stdout);
    time_t secs=time(0);
    tm *t=localtime(&secs);
    char ch[30];
    sprintf(ch,"%04d-%02d-%02d %02d-%02d.txt",t->tm_year+1900,t->tm_mon+1,t->tm_mday,t->tm_hour,t->tm_min);
    //sprintf(ch,"%04d-%02d-%02d.txt",t->tm_year+1900,t->tm_mon+1,t->tm_mday);
    string line,time_str = ch;
    srand(time(NULL));
    ofstream questions, pareto_file, nonPareto_file;
    ofstream graphviz[CRITERIA_SIZE];
    ifstream croud_result, grnd_trth;
    int i,c;
    map<int,char> preference_symbol;
    preference_symbol[0] = '<';
    preference_symbol[1] = '>';
    preference_symbol[2] = '~';
    graphs = new preference_graph[CRITERIA_SIZE];
    forl(i,0,CRITERIA_SIZE){
        graphs[i].resetAll();
    }
    dgraph.resetAll();
    forl(i,0,OBJECT_SIZE){
        remaining_objects.insert(i);
    }
    forl(i,0,CRITERIA_SIZE){
        char index[30];
        sprintf(index,"%d.txt",i);
        string ind = index;
        graphviz[i].open((string ("criterion_"+ind)).c_str());
        graphviz[i] << "digraph {\n";
    }
    questions.open((string ("questions_"+time_str)).c_str());
    pareto_file.open((string ("pareto_file_"+time_str)).c_str());
    nonPareto_file.open((string ("nonPareto_file_"+time_str)).c_str());
    croud_result_output.open((string ("croud_result_output_"+time_str)).c_str());
    croud_result.open("croud_result_.txt");
    grnd_trth.open("ground_truth.txt");
    pareto_file << "Pareto Optimal Objects:\n\n";
    nonPareto_file << "Non Pareto Optimal Objects:\n\n";
    pii randomP=make_pair(-1,-1);
    pii prevPair = make_pair(-1,-1);
    int question_cnt=0;
    includeNon_pareto = false;
    ms(ground_truth,-1);
    int x,y,r,criterion;
    while(getline(grnd_trth,line)){
        char tmp_arr[30];
        strcpy(tmp_arr, line.c_str());
        sscanf(tmp_arr,"%d %d %d %d",&c,&x,&y,&r );
        if(ground_truth[c][x][y] != -1){
            cout << c << " " << x << " " << y << endl;
            continue;
        }
        if(r == 2){
            ground_truth[c][x][y] = ground_truth[c][y][x] = r;
        }else{
            ground_truth[c][x][y] = r;
            ground_truth[c][y][x] = 1^r;
        }
    }
    while(!remaining_objects.empty()){
        vector<int> question;
        if(includeNon_pareto == false){
            includeNon_pareto = NeedNon_pareto();
        }
        //RandomQ
        if(QUESTION_SELECTION_ALGO == 0){
            question = generate_question_RandomQ();
            while(question.size() == 0){
                question = generate_question_RandomQ();
            }
        }else if(QUESTION_SELECTION_ALGO == 1){ //RandomP
            if(randomP.first == -1){
                randomP = getRandomPair();
            }
            print_pair(randomP);
            int cr = getCriterion_OnPair(randomP.first, randomP.second);
            if(cr == -1){ //no criteria to choose for current x,y
                randomP = make_pair(-1,-1); //choose a different pair next iteration
                continue;
            }
            question.pb(randomP.first);
            question.pb(randomP.second);
            question.pb(cr);
        }else if(QUESTION_SELECTION_ALGO == 2){ //FRQ
            pii p1 = getFRQ_pair();
            vector<pii> vpii; //(criteria, score) pair
            int x = p1.first;
            int y = p1.second;
            forl(c,0,CRITERIA_SIZE){
                if(!(graphs[c].doesRelationExist(x,y) || graphs[c].doesRelationExist(y,x))){
                    int rc = dgraph.get_dominated_by(y).size();
                    rc += dgraph.get_indifference_with(y).size();
                    rc -= dgraph.get_domination_over(y).size();
                    rc -= dgraph.get_dominated_by(x).size();
                    rc -= dgraph.get_indifference_with(x).size();
                    rc += dgraph.get_domination_over(x).size();
                    vpii.pb(make_pair(c,rc));
                }
            }
            sort(all(vpii),score_comp);
            //vpii[0].first is the criterion with best score
            question.pb(x),question.pb(y),question.pb(vpii[0].first);
        }
        print(question);
        int criterion = question[2];
        int res;
        //res = CroudResult();
        //croud_result >> res;
        res = ground_truth[criterion][question[0]][question[1]];
        int initial_res = res;
        if(res == 2){
            graphs[criterion].set_relation(question[0],question[1], INDIFFERENT_ON_C);
        }else{
            graphs[criterion].set_relation(question[1],question[0], res);
        }

        resolve_contradiction(question);
        res = graphs[criterion].getPreferenceValue(question[1], question[0]);
        if(initial_res != res){
            //croud_result_output << "resolve on criterion " << criterion << endl;
            //croud_result_output << initial_res << " " << res << endl;
        }
        asked_questions[question_cnt].pb(question[0]),asked_questions[question_cnt].pb(question[1]),asked_questions[question_cnt].pb(question[2]);

        //transitive closure
        graphs[criterion].find_transitive_closure();
        set<int>::iterator it;
        vector<int> toBeRemoved;
        for(it = remaining_objects.begin(); it != remaining_objects.end(); it++){
            int obj = *it;
            pair<bool,int> np_res = checkIfNon_pareto_optimal(obj);
            if(np_res.first == true){
                toBeRemoved.pb(obj);
                nonPareto_file << char('a'+obj) << " (dominated by " << char('a'+np_res.second) << ")"<<  endl;
                notPareto.insert(obj);
            }else if(checkIfPareto_Optimal(obj)){
                toBeRemoved.pb(obj);
                pareto_file << char('a'+obj) << endl;
                pareto.insert(obj);
            }
        }
        int vsize = toBeRemoved.size();
        forl(i,0,vsize){
            remaining_objects.erase(toBeRemoved[i]);
        }
        question_cnt++;
    }
    forl(i,0,question_cnt){
        x = asked_questions[i][0], y = asked_questions[i][1];
        criterion = asked_questions[i][2];
        int ground_truth_value = ground_truth[criterion][x][y];
        int output_value = graphs[criterion].getPreferenceValue(y,x);
        croud_result_output << char('a' + x) << " ? " << char('a' + y) << " over " <<  criterion << endl;
        if(ground_truth_value != output_value){
            croud_result_output << "Ground truth value: ";
            croud_result_output << char('a' + x) << " " << preference_symbol[ground_truth_value] << " " << char('a' +y);
            croud_result_output << " Program value: " << char('a' + x) << " " << preference_symbol[output_value] << " " << char('a'+y) << endl;
        }else{
            croud_result_output << "Both Ground truth & Program value: " << char('a' + x) << " " << preference_symbol[output_value] << " " << char('a'+y) << endl;
        }
        croud_result_output << endl;

        if(output_value == PREFERRED_OVER){ //y>x
            graphviz[criterion] << "\t\t" << char('a' + y) << " -> " << char('a' + x) << ";\n";
        }else if(output_value == NOT_PREFERRED_OVER){ //x>y
            graphviz[criterion] << "\t\t" << char('a' + x) << " -> " << char('a' + y) << ";\n";
        }
    }
    forl(i,0,CRITERIA_SIZE){
        graphviz[i] << "}\n";
    }
    questions.close();
    pareto_file.close();
    nonPareto_file.close();
    return 0;
}

digraph {
		b -> a;
		c -> b;
		b -> d;
		e -> b;
		b -> f;
		g -> b;
		b -> h;
		i -> b;
		b -> j;
		c -> e;
		g -> c;
		g -> i;
		i -> e;
		i -> c;
}

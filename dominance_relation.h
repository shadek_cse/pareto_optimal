#ifndef DOMINANCE_RELATION_H_INCLUDED
#define DOMINANCE_RELATION_H_INCLUDED

class dominance_graph{
public:
    int dominance_value[OBJECT_SIZE][OBJECT_SIZE];
    set<int> domination_over[OBJECT_SIZE];
    set<int> indifference_with[OBJECT_SIZE];
    set<int> dominated_by[OBJECT_SIZE];
    void resetAll(){
        memset(dominance_value,-1,sizeof(dominance_value));
    }
    void set_dominance(int obj1, int obj2,int value){
        if(value == INDIFFERENT){
            dominance_value[obj1][obj2] = dominance_value[obj2][obj1] = INDIFFERENT;
            indifference_with[obj1].insert(obj2), indifference_with[obj2].insert(obj1);
        }else{
            dominance_value[obj1][obj2] = value;
            dominance_value[obj2][obj1] = 1^value; //value is opposite in other direction
            int a=obj1,b=obj2;
            if(value==1){
                swap(a,b);
            }
            domination_over[a].insert(b),dominated_by[b].insert(a);
        }
    }
    int get_dominance(int obj1, int obj2){
        return dominance_value[obj1][obj2];
    }
    set<int> get_domination_over(int index){
        return domination_over[index];
    }
    set<int> get_indifference_with(int index){
        return indifference_with[index];
    }
    set<int> get_dominated_by(int index){
        return dominated_by[index];
    }
};

#endif // DOMINANCE_RELATION_H_INCLUDED
